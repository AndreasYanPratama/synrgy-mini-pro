'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class Parent extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = ({ nama, email, password, role, provinsi, kabupaten, kecamatan, kelurahan, alamat, pendidikan_anak }) => {
      if (nama) return Promise.reject("User not found !!")
      const encryptedPassword = this.#encrypt(password);

      return this.create({ nama, email, password : encryptedPassword, role, provinsi, kabupaten, kecamatan, kelurahan, alamat, pendidikan_anak })
    }

    /* Method .compareSync digunakan untuk mencocokkan plaintext dengan hash. */
    checkPassword = password => bcrypt.compareSync(password, this.password)
    /* Method ini kita pakai untuk membuat JWT*/
    generateToken = () => {
      //Jangan memasukkan password ke dalam payload
      const payload = {
        id: this.id,
        email: this.email
      }
      // Rahasia ini nantinya kita pakai untuk memverifikasi apakah token ini benar-benar berasal dari aplikasi kita
      const rahasia = 'Ini rahasia gak boleh disebar-sebar'
      // Membuat token dari data-data diatas
      const token = jwt.sign(payload, rahasia)
      return token
    }

    /* Method Authenticate, untuk login */
    static authenticate = async ({ email, password })=>{
      try{
        const user = await this.findOne({ where: { email } })
        if (!user) return Promise.reject("Parent's account not found !!")
        
        const isPasswordValid = user.checkPassword(password)
        if (!isPasswordValid) return Promise.reject("Wrong password")
        
        return Promise.resolve(user)
      }catch(err){
        return Promise.reject(err)
      }
    }

  };
  Parent.init({
    nama: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING,
    provinsi: DataTypes.STRING,
    kabupaten: DataTypes.STRING,
    kecamatan: DataTypes.STRING,
    kelurahan: DataTypes.STRING,
    alamat: DataTypes.STRING,
    pendidikan_anak: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Parent',
  });
  return Parent;
};