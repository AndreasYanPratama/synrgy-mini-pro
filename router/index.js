const router =require("express").Router();

const parentRouter = require('./parentRouter');

router.use('/parent',parentRouter);
module.exports = router;