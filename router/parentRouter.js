// router.js
const router = require ('express' ).Router ()
// Controllers
const parent = require ('../controllers/parentController' )
const restrict  = require('../middlewares/restrict')

// Register Page
router.get('/register' , (req, res) => res.render ('register' ))
router.post('/register' , parent.register )

router.get('/login', (req, res) => res.render ('login' ))
router.post('/login', parent.login)

router.get('/dashboard', restrict, parent.dashboard)

module .exports = router;