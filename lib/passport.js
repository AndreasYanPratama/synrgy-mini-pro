const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
// const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const { Parent } = require('../models')

// var cookieExtractor = function(req) {
//     var token = null;
//     if (req && req.cookies) token = req.cookies['jwt'];
//     return token;
// };
 
/* Passport JWT Options */
// const options = {
//     // Untuk mengekstrak JWT dari request, dan mengambil token-nya dari header yang bernama Authorization
//     // jwtFromRequest : ExtractJwt .fromHeader ('authorization'),
//     jwtFromRequest : cookieExtractor,
//     /* Harus sama seperti dengan apa yang kita masukkan sebagai parameter kedua dari jwt.sign di Parent Model.
//     Inilah yang kita pakai untuk memverifikasi apakah tokennya dibuat oleh sistem kita */
//     secretOrKey : 'Ini rahasia gak boleh disebar-sebar'
// }

/* Fungsi untuk authentication */
async function authenticate(email, password, done){
    try {
        // Memanggil method kita tadi
        const parent = await Parent.authenticate({ email, password })

        return done(null, parent)
    } catch (err) {
        return done(null, false, { message: err })
    }
}

// passport .use(new JwtStrategy (options, async (payload, done) => {
//     // payload adalah hasil terjemahan JWT, sesuai dengan apa yang kita masukkan di parameter pertama dari jwt.sign
//     Parent.findByPk (payload .id)
//     .then(parent => done(null, parent))
//     .catch(err => done(err, false))
// }))

passport.use(
    new LocalStrategy({ usernameField: 'email', passwordField: 'password' }, authenticate)
)

/* Serialize dan Deserialize
   Cara untuk membuat sesi dan menghapus sesi
*/
passport.serializeUser(
    (parent, done) => done(null, parent.id)
)
passport.deserializeUser(
    async (id, done) => done(null, await Parent.findByPk(id))
)

module.exports = passport