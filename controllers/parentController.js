const { Parent } = require('../models');
const passport = require('../lib/passport');

module.exports = {
    register: (req, res, next) => {
        Parent.register(req.body).then(() => {
            res.redirect('login')
        }).catch(err=> next(err))
    },

    login: passport.authenticate('local', {
        successRedirect: 'dashboard',
        failureRedirect: 'login',
        failureFlash: true
    }),

    dashboard: (req, res) => {
        const currentUser = req.user.nama;
        console.log(currentUser);
        res.render('dashboard', { currentUser})
        
    },
}